package ru.karamyshev.taskmanager.listener.info;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.util.NumberUtil;

@Component
public class SystemInfoListener extends AbstractListener {

    @NotNull
    @Override
    public String arg() {
        return "-i";
    }

    @NotNull
    @Override
    public String command() {
        return "info";
    }

    @NotNull
    @Override
    public String description() {
        return "Display information about system.";
    }

    @Override
    @EventListener(condition = "@systemInfoListener.command() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("\n [INFO]");

        @Nullable final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);

        @Nullable final long freeMemory = Runtime.getRuntime().freeMemory();
        @Nullable final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory (bytes): " + freeMemoryFormat);

        @Nullable final long maxMemory = Runtime.getRuntime().maxMemory();
        @Nullable final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        @Nullable final String maxMemoryFormat = (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue);
        System.out.println("Maximum memory (bytes): " + maxMemoryFormat);

        @Nullable final long totalMemory = Runtime.getRuntime().totalMemory();
        @Nullable final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM (bytes): " + totalMemoryFormat);

        @Nullable final long usedMemory = totalMemory - freeMemory;
        @Nullable final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM (bytes): " + usedMemoryFormat);
    }

}
