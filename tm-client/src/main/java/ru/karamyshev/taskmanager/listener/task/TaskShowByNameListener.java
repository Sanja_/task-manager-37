package ru.karamyshev.taskmanager.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.endpoint.*;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.service.SessionService;
import ru.karamyshev.taskmanager.util.TerminalUtil;

import java.lang.Exception;
import java.util.List;

@Component
public class TaskShowByNameListener extends AbstractListener {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String arg() {
        return "-tskvwnm";
    }

    @NotNull
    @Override
    public String command() {
        return "task-view-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by name.";
    }

    @Override
    @EventListener(condition = "@taskShowByNameListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        final SessionDTO session = sessionService.getSession();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskEndpoint.findTaskOneByName(session, name);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);

        System.out.println("[OK]");
    }

    private void showTask(@Nullable final Task task) {
        if (task == null) return;
        System.out.println("\n");
        System.out.println("ID:" + task.getId());
        System.out.println("NAME:" + task.getName());
        System.out.println("DESCRIPTION:" + task.getDescription());
    }

}
