package ru.karamyshev.taskmanager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.entity.AbstractEntity;

@Component
public abstract class AbstractService<E extends AbstractEntity> {

    @Autowired
    protected AnnotationConfigApplicationContext context;

}
