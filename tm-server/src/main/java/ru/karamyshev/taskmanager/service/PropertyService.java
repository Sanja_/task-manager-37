package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.karamyshev.taskmanager.api.service.IPropertyService;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Service
public class PropertyService implements IPropertyService {

    private final String file = "/application.properties";

    private final Properties properties = new Properties();

    {
        init();
    }

    @Override
    public void init() {
        final InputStream inputStream = PropertyService.class.getResourceAsStream(file);
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @NotNull
    @Override
    public String getServerHost() {
        @NotNull final String propertyHost = properties.getProperty("server.host");
        @NotNull final String envHost = System.getProperty("server.host");
        if (envHost != null) return envHost;
        return propertyHost;
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        @NotNull final String propertyPort = properties.getProperty("pdb.port");
        @NotNull final String envPort = System.getProperty("pdb.port");
        System.out.println("SERVER PORT " + envPort);
        @NotNull String value = propertyPort;
        if (envPort != null) value = envPort;
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getSessionSalt() {
        return properties.getProperty("session.salt");
    }

    @NotNull
    @Override
    public Integer getSessionCycle() {
        return Integer.parseInt(properties.getProperty("session.cycle"));
    }

    @NotNull
    @Override
    public String getJdbcDriver() {
        return properties.getProperty("pdb.driver");
    }

    @NotNull
    @Override
    public String getJdbcUrl() {
        return properties.getProperty("pdb.url");
    }

    @NotNull
    @Override
    public String getJdbcUsername() {
        return properties.getProperty("pdb.login");
    }

    @NotNull
    @Override
    public String getJdbcPassword() {
        return properties.getProperty("pdb.password");
    }

}
