package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.karamyshev.taskmanager.repository.ISessionRepository;
import ru.karamyshev.taskmanager.api.service.IPropertyService;
import ru.karamyshev.taskmanager.api.service.ISessionService;
import ru.karamyshev.taskmanager.api.service.IUserService;
import ru.karamyshev.taskmanager.dto.SessionDTO;
import ru.karamyshev.taskmanager.dto.UserDTO;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;
import ru.karamyshev.taskmanager.exception.user.AccessDeniedException;
import ru.karamyshev.taskmanager.util.HashUtil;
import ru.karamyshev.taskmanager.util.SignatureUtil;

import java.util.List;

@Service
public class SessionService extends AbstractService<Session> implements ISessionService {

    @Autowired
    private IUserService userService;

    @Autowired
    private IPropertyService propertyService;

    @Autowired
    private ISessionRepository sessionRepository;

    @Autowired
    private ISessionService sessionService;

    @Override
    @Transactional
    public void close(@Nullable final SessionDTO sessionDTO) throws Exception {
        validate(sessionDTO);
        sessionRepository.deleteById(sessionDTO.getId());
    }

    @Override
    @Transactional
    public void remove(@Nullable Session session) throws Exception {
        if (session == null) return;
        sessionRepository.delete(session);
    }

    @Override
    @Transactional
    public void closeAll(@Nullable final SessionDTO session) throws Exception {
        validate(session);
        sessionRepository.removeByUserId(session.getUserId());
    }

    @Nullable
    @Override
    public UserDTO getUser(@NotNull final SessionDTO session) throws Exception {
        @NotNull final String userId = getUserId(session);
        if (userId == null) return null;
        return UserDTO.toDTO(userService.findById(userId));
    }

    @Nullable
    @Override
    public String getUserId(@Nullable final SessionDTO session) throws Exception {
        validate(session);
        return session.getUserId();
    }

    @Nullable
    @Override
    public List<SessionDTO> getListSessionUser(@Nullable final SessionDTO session) throws Exception {
        sessionService.validate(session);
        @NotNull final List<SessionDTO> sessionDTOList =
                SessionDTO.toDTO(sessionRepository.findAllByUserId(session.getUserId()));
        return sessionDTOList;
    }

    @Override
    public boolean isValid(@Nullable final SessionDTO session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    @Transactional
    public void validate(@Nullable final SessionDTO session) throws Exception {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getStartTime() == null) throw new AccessDeniedException();
        final SessionDTO temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        final String signatureSource = session.getSignature();
        final String signatureTarget = sign(temp);
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        if (!sessionRepository.existsById(session.getId())) throw new AccessDeniedException();
    }

    @Override
    @Transactional
    public void validate(
            @Nullable final SessionDTO session,
            @Nullable final Role role
    ) throws Exception {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        final String userId = session.getUserId();
        final UserDTO user = UserDTO.toDTO(userService.findById(userId));
        if (user == null) throw new AccessDeniedException();
        if (user.getRole() == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Nullable
    @Override
    @Transactional
    public SessionDTO open(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) return null;
        @NotNull final Session session = new Session();
        session.setUser(user);
        session.setStartTime(System.currentTimeMillis());
        session.setSignature(sign(SessionDTO.toDTO(session)));
        sessionRepository.save(session);
        return SessionDTO.toDTO(session);
    }

    @Nullable
    @Override
    public String sign(@Nullable final SessionDTO session) {
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        return signature;
    }

    @Override
    public boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final UserDTO user = UserDTO.toDTO(userService.findByLogin(login));
        if (user == null) return false;
        final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    @Transactional
    public void signOutByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new AccessDeniedException();
        final UserDTO user = UserDTO.toDTO(userService.findByLogin(login));
        if (user == null) throw new AccessDeniedException();
        final String userId = user.getId();
        sessionRepository.removeByUserId(userId);
    }

    @Override
    @Transactional
    public void signOutByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        sessionRepository.removeByUserId(userId);
    }

}
