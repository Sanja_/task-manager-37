package ru.karamyshev.taskmanager.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.dto.ProjectDTO;
import ru.karamyshev.taskmanager.entity.Project;
import ru.karamyshev.taskmanager.repository.IProjectRepository;

import java.util.List;

@RestController
@RequestMapping(value = "/project")
public class ProjectController {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private IProjectRepository projectRepository;

    @GetMapping(value = "/projects", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProjectDTO> findAll() {
        return projectService.findAll();
    }

    @DeleteMapping(value = "/delete/{id}")
    public void delete(@PathVariable("id") @Nullable String id) throws Exception {
        projectService.removeOneById(id);
    }

    @GetMapping(value = "/view/{id}", produces = MediaType.APPLICATION_XML_VALUE)
    public ProjectDTO view(@PathVariable("id") String id) throws Exception {
        @Nullable final Project project = projectService.findById(id);
        return ProjectDTO.toDTO(project);
    }

    @PostMapping("/edit/{id}")
    public void edit(
            @RequestParam("id") @Nullable final String projectId,
            @RequestBody @Nullable Project project
    ) {
        projectService.updateProjectById(projectId, project);
    }

    @PostMapping(value = "/create")
    public void create(
            @RequestBody @Nullable final Project project
    ) throws Exception {
        projectService.create(project);
    }

}
