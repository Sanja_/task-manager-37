package ru.karamyshev.taskmanager.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_user")
public class User extends AbstractEntity {

    private static final long serialVersionUID = 1001L;

    @NotNull
    @Column(
            unique = true,
            nullable = false,
            updatable = false
    )
    private String login = "";

    @NotNull
    @Column(
            nullable = false
    )
    private String passwordHash = "";

    @Nullable
    @Column(columnDefinition = "TEXT")
    private String email = "";

    @Nullable
    @Column(columnDefinition = "TEXT")
    private String firstName;

    @Nullable
    @Column(columnDefinition = "TEXT")
    private String lastName = "";

    @Nullable
    @Column(columnDefinition = "TEXT")
    private String middleName = "";

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Role role = Role.USER;

    @NotNull
    @Column
    private Boolean locked = false;

    @Nullable
    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Project> projects= new ArrayList<>();

    @Nullable
    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Task> tasks= new ArrayList<>();

    @Nullable
    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Session> sessions= new ArrayList<>();

}
