package ru.karamyshev.taskmanager.repository;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.Session;

import java.util.List;
import java.util.Optional;


public interface ISessionRepository extends IRepository<Session> {

    void removeAllByUserId(@Nullable Session session);

    @Nullable
    List<Session> findAllByUserId(@Nullable String userId) throws Exception;

    @Nullable
    Optional findById(@Nullable String id);

    void removeByUserId(@Nullable String userId) throws Exception;

}
