package ru.karamyshev.taskmanager.service;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.api.service.IUserService;
import ru.karamyshev.taskmanager.dto.ProjectDTO;
import ru.karamyshev.taskmanager.entity.Project;
import ru.karamyshev.taskmanager.exception.empty.EmptyUserIdException;
import ru.karamyshev.taskmanager.exception.empty.IdEmptyException;
import ru.karamyshev.taskmanager.exception.empty.NameEmptyException;
import ru.karamyshev.taskmanager.repository.IProjectRepository;

import java.util.List;

@Service
public class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @Override
    @Transactional
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new Exception();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(userService.findById(userId));
        projectRepository.save(project);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final List<Project> projectList =
                (projectRepository.findAllByUserId(userId));
        return ProjectDTO.toDTO(projectList);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll() {
        @NotNull final List<Project> projectList = projectRepository.findAll();
        return ProjectDTO.toDTO(projectList);
    }

    @Override
    @Transactional
    public void clearAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        projectRepository.deleteAllByUserId(userId);
    }

    @Nullable
    @Override
    public Project findOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Project project = projectRepository.findByUserIdAndName(userId, name);
        return project;
    }

    @Override
    @Transactional
    public void updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return;
        project.setName(name);
        project.setDescription(description);
        projectRepository.save(project);

    }

    @Override
    @Transactional
    public void removeOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        projectRepository.deleteByUserIdAndName(userId, name);
    }

    @Nullable
    @Override
    public Project findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = projectRepository.findByUserIdAndId(userId, id);
        return project;
    }

    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        projectRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void removeAll() {
        projectRepository.deleteAll();
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        projectRepository.deleteById(id);
    }

    @Nullable
    @Override
    public Project findById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @Nullable final Project project = projectRepository.findById(id).orElse(null);
        return project;
    }

    @Override
    @Transactional
    public void updateProjectById(
            @Nullable final String id,
            @Nullable final Project project
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (project == null) throw new NameEmptyException();
        @Nullable final Project upProject = projectRepository.findById(id).orElse(null);
        if (upProject == null) return;
        upProject.setName(project.getName());
        upProject.setDescription(project.getDescription());
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void create(
            @Nullable Project project
    ) throws Exception {
        if (project == null) throw new Exception();
        projectRepository.save(project);
    }

}
