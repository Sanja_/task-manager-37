<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../resources/_header.jsp"/>


    <head>Update Project</head>
    <body>
        <h3>Create new project</h3>
                <form:form method="POST"
                     action="/project/edit/${project.id}" modelAttribute="project">
                        <table>
                           <tr>
                               <td><form:label path="name">Name</form:label></td>
                               <td><form:input path="name"/></td>
                           </tr>
                           <tr>
                               <td><form:label path="description">
                                 Contact Number</form:label></td>
                               <td><form:input path="description"/></td>
                           </tr>
                           <tr>
                               <td><input type="submit" value="SAVE PROJECT"/></td>
                           </tr>
                       </table>
                 </form:form>

    </body

<jsp:include page="../resources/_footer.jsp"/>