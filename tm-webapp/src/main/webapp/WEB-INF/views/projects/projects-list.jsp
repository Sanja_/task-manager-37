<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../resources/_header.jsp"/>

<h1>Project List</h1>


    <table width="100%" cellpadding="10" border="2" style="border-collapse: collapse;">

        <tr>
            <th width="200">ID</th>
            <th width="200">NAME</th>
            <th width="200">DESCRIPTION</th>
            <th width="100" align="center">VIEW</th>
            <th width="100" align="center">EDIT</th>
            <th width="100" align="center">DELETE</th>
        </tr>

        <c:forEach var="project" items="${projects}">
            <td><c:out value="${project.id}"/></td>
            <td><c:out value="${project.name}"/></td>
            <td><c:out value="${project.description}"/></td>

            <td align="center">
                <a href="/project/view/${project.id}/">VIEW</a>
            </td>

            <td align="center">
                <a href="/project/edit/${project.id}/">EDIT</a>
            </td>

            <td align="center">
                <a href="/project/delete/${project.id}/">DELETE</a>
            </td>
           </tr>
        </c:forEach>
    </table>

      <form action="/project/create">
        <input type="submit" value="CREATE PROJECT">
      </form>

<jsp:include page="../resources/_footer.jsp"/>