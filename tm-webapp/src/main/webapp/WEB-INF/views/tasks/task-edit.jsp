<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../resources/_header.jsp"/>


    <head>Create new project</head>
    <body>
                <form:form method="POST"
                     action="/task/edit/${task.id}" modelAttribute="task">
                        <table>
                           <tr>
                               <td><form:label path="name">Name</form:label></td>
                               <td><form:input path="name"/></td>
                           </tr>
                           <tr>
                               <td><form:label path="description">
                                 Contact Number</form:label></td>
                               <td><form:input path="description"/></td>
                           </tr>
                           <tr>
                               <td><input type="submit" value="SAVE TASK"/></td>
                           </tr>
                       </table>
                 </form:form>

    </body

<jsp:include page="../resources/_footer.jsp"/>