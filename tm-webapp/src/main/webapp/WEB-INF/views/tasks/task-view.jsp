<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../resources/_header.jsp"/>

    <body>
        <h3>Task View</h3>
        <table>
            <tr>
                <td>Name :</td>
                <td>"${task.name}"</td>
            </tr>

            <tr>
                <td>Name Project :</td>
                <td>"${task.project.name}"</td>
            </tr>

            <tr>
                <td>Description :</td>
                <td>"${task.description}"</td>
            </tr>
        </table>
    </body

<jsp:include page="../resources/_footer.jsp"/>